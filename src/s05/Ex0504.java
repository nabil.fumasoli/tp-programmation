package s05;

import proglib.SimpleIO;

public class Ex0504 {
    public static void main(String[] args) {
        int n = SimpleIO.readInt("Entrez un nombre entier positif");
        int divider = 2, i = 0;
        System.out.print(n + " = ");
        while (n >= 2) {
            while (n % divider == 0) {
                n = n / divider;
                if (i == 0) {
                    System.out.print(divider);
                } else {
                    System.out.print(" x " + divider);
                }
                i++;
            }
            divider++;
        }
    }
}