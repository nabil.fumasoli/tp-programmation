package s05;

public class Ex0503 {
    public static void main(String[] args) {
        double e = 1;
        double value;
        int i = 1;
        double f = 1;
        do {
            f *= i;
            value = 1 / f; // or value = 1/factorial(i);
            e = e + value;
            i++;
        } while (value >= Math.pow(10, -12));
        System.out.println(e);
    }

    public static double factorial(int fac) {
        double number = 1;
        int i = 2;
        while (i <= fac) {
            number = number * i;
            i++;
        }
        return number;
    }
}