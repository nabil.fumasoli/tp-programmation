package s05;

import proglib.SimpleIO;

public class Ex0502 {
    public static void main(String[] args) {
        int teeth = SimpleIO.readInt("Entrez le nombre de dents");
        int height = SimpleIO.readInt("Entrez la hauteur souhaitée");
        for (int i = 0; i < teeth; i++) {
            for (int j = 0; j < height; j++) {
                for (int k = 0; k < j; k++) {
                    System.out.print("*");
                }
                System.out.println("*");
            }
        }
    }
}