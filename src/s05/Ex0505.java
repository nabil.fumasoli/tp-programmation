package s05;

public class Ex0505 {
    public static void main(String[] args) {
        int x = 50;
        int number = 0;
        while (x <= 1000) {
            if (x % 5 == 0 && x % 6 == 0) {
                number++;
                switch (number) {
                    case 1:
                        System.out.print(x);
                        break;
                    case 10:
                        System.out.println("," + x);
                        number = 0;
                        break;
                    default:
                        System.out.print("," + x);
                }
            }
            x++;
        }
    }
}
