
package s21;

public class PlayWithSeries {
  
  public static abstract class Series {
    public abstract double function(int i);
    public double evaluateBetween(int from, int to) {
      double sum=0; 
      for(int i=from; i<=to; i++) {
        sum += function(i);
      }
      return sum;
    }
  }
  //=================================================
  // static class SeriesB1 // TODO...
  
  // static class SeriesB2 // TODO...

  
  //=================================================

  public static void main(String[] args) {
    // *******************************************
    // ****** With internal static classes: ******
    // *******************************************

    // TODO... create instances of SeriesB1/B2, evaluate and print the result
    // Series sa=new SeriesB1();
    // Series sb=new SeriesB2();
    // System.out.println(sa.evaluateBetween(0, 20));
    // System.out.println(sb.evaluateBetween(0, 20));    
    // System.out.println();

    
    // *********************************************
    // ****** With anonymous classes: (ex. 3) ******
    // *********************************************

    // TODO... create equivalent instances, evaluate and print the result
    
  }
  
}
