
package s21;
import static s21.PlayWithFunctions.*;

public class FoldOperators {
  // ***********************************
  // ****** With internal classes ******
  // ***********************************

  //public static final IFoldableOperation SUM = new SumOperation();
  //public static final IFoldableOperation PRODUCT = new ...
  //public static final IFoldableOperation MAX =...
  
  //private static class SumOperation ...

  //private static class ProductOperation ...

  //private static class MaxOperation ...
  
  
  
  // ********************************************
  // ****** With anonymous classes (ex. 3) ******
  // ********************************************

  //public static final IFoldableOperation SUM1 =...
  
  //public static final IFoldableOperation PRODUCT1 =...
  
  //public static final IFoldableOperation MAX1 =...


}