package s06;

import proglib.SimpleIO;

public class Ex0606 {
    public static void main(String[] args) {
        int number = SimpleIO.readInt("Entrez un nombre entier");
        int calculate = sumDigits(number);

        System.out.println("Somme des nombres du chiffre entré (" + number + "): " + calculate);
    }

    public static int sumDigits(int n) {
        int sum = 0;
        while (n != 0) {
            sum = sum + n % 10;
            n = n / 10;
        }
        return sum;
    }
}