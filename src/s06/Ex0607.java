package s06;

import static java.lang.Math.min;

public class Ex0607 {
    public static void main(String[] args) {
        int pgdc = pgdc(12, 10);
        System.out.println(pgdc);
    }

    public static int pgdc(int x, int y) {
        int modulo = 0;
        int min = min(x, y);
        while (x > y) {
            modulo = x % y;
            x = y / modulo;
            System.out.println("min " + min);
        }
        return modulo;
    }
}