package s20.ex2_games.cards;

import s20.ex2_games.ItemList;
import s20.ex2_games.Playable;

import java.security.SecureRandom;

public class CardDeck extends ItemList {

    private final static int NUMBER_OF_CARDS = 52;

    public CardDeck(int shuffles) {
        items = new Card[NUMBER_OF_CARDS];
        int i = 0;
        for (Suit s : Suit.values()) {
            for (Face f : Face.values()) {
                items[i++] = new FaceCard(s, f);
            }
            for (int r = 2; r <= 10; r++) {
                items[i++] = new NumberCard(s, r);
            }
        }
        if (i != NUMBER_OF_CARDS) {
            throw new RuntimeException("Error with cards");
        }
        this.Shuffle(shuffles);
    }

    void Shuffle(int n) {
        int len = items.length;
        Playable[] temp = new Card[len];
        SecureRandom r = new SecureRandom();
        for (int c = 0; c < n; c++) {
            int cut1 = r.nextInt(len);
            int cut2 = r.nextInt(len);
            if (cut2 < cut1) {
                int t = cut1;
                cut1 = cut2;
                cut2 = t;
            }
            for (int i = cut1; i < cut2; i++) {
                temp[i - cut1] = this.items[i];
            }
            for (int i = 0; i < cut1; i++) {
                temp[i + cut2 - cut1] = this.items[i];
            }
            for (int i = 0; i < cut2; i++) {
                this.items[i] = temp[i];
            }
        }
    }
}