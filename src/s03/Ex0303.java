//==============================================================================
//  HEIA-FR / Jacques Bapst
//==============================================================================

package s03;

public class Ex0303 {
  public static void main(String [] args) {
    int i; 
    float f=4.8f; 
    double z, d=0.6;
    char c='3';

    i=(int)f;         // i = 4
    i=c;              // i = 51
    z=1/4;            // z = 0,0
    c=(char)(110*d);  // c = B (66)
    i=(byte)(f*60);   // i = 32
    i=(byte)f*60;     // i = 240
    
    System.out.println(i);
  }
}
