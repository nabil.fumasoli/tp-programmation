package s03;

public class Ex0306 {
    public static void main(String[] args) {
        int a = 8;
        int b = 7;
        int c = 7;
        int r = 0;

        if (a >= b && a >= c) {
            r = a;
        } else if (b >= c && b >= a) {
            r = b;
        } else if (c >= a && c >= b) {
            r = c;
        }
        System.out.println(r);
    }
}
