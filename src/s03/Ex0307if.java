package s03;

public class Ex0307if {
    public static void main(String[] args) {
        int day = 6;
        if (day <= 5) {
            System.out.println("Working day");
        } else if (day == 6 | day == 7) {
            System.out.println("Week-end");
        } else {
            System.out.println("Invalid day");
        }
    }
}
