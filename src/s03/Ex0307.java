package s03;

public class Ex0307 {
    public static void main(String[] args) {
        int day = 1;
        switch (day) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                System.out.println("Working day");
                break;
            case 6:
            case 7:
                System.out.println("Week-end");
                break;
            default:
                System.out.println("Invalid day");
        }


    }
}
