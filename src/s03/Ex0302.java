package s03;

public class Ex0302 {
    public static void main(String[] args) {
        int a = 0, b = 1, c = 2, d = 3, x;
        if (a < b) // a < b
            if (c < d) // a < b && c < d
                x = 1;
            else // a < b && c >= d
                if (a < c) // a < b && a < c && c >= d
                    if (b < d) // a < b && a < c && b < d && c >= d
                        x = 2;
                    else // a < b && a < c && b > d && c >= d
                        x = 3;
                else // a < b && a >= c && c >= d
                /**
                 * a is smaller than b
                 * a is bigger than or equals to c and bigger than or equals to d, so a is bigger than or equals to d
                 * (a < b)
                 * (a >= c >= d)
                 */
                /**
                    if (a < d) // a < b && a < d && c >= d
                        if (b > c) // a < b && a < d && b > c && c >= d
                            x = 4;
                        else // a < b && a < d && b > c && c >= d
                            x = 5;
                    else // a > d && c >= d
                 **/
                        x = 6;
        else // a > b
            x = 7;
    }
}