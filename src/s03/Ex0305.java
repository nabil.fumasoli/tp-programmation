package s03;

import proglib.SimpleIO;

public class Ex0305 {
    public static void main(String[] args) {
        //Criterias: n / 6 if divisible by 2 and 3. n / 5 if it ends with 0 or 5
        int n = SimpleIO.readInt("Veuillez entrer un nombre entier");
        if (n % 5 == 0 && n % 6 == 0) {
            System.out.println(n + " est divisible par 5 et 6");
        } else if (n % 5 == 0) {
            System.out.println(n + " est divisible par 5");
        } else if (n % 6 == 0) {
            System.out.println(n + " est divisible par 6");
        } else {
            System.out.println(n + " n'est ni divisible par 5, ni par 6");
        }
    }
}