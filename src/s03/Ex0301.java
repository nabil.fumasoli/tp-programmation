package s03;

import proglib.SimpleIO;

public class Ex0301 {
    public static void main(String[] args) {
        int a = SimpleIO.readInt("Choisissez une valeur entière pour un PETIT côté ab: ");
        int b = SimpleIO.readInt("Choisissez une valeur entière pour un PETIT côté bc: ");
        int c = SimpleIO.readInt("Choisissez une valeur entière pour un GRAND côté ca: ");
        System.out.println("ab = " + a + " bc = " + b + " ca = " + c);
        if((a + b) <= c){
            System.out.println("Erreur: le triangle n'existe pas ! Veuillez réessayer");
        } else {
            System.out.println("Triangle constructible. Voici son périmètre: " + (a + b + c) + " cm.");
        }
    }
}