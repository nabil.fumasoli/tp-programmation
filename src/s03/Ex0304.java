//==============================================================================
//  HEIA-FR / Jacques Bapst
//==============================================================================

package s03;

import proglib.SimpleIO;

public class Ex0304 {

  //----------------------------------------------------------------------------
  // Values a, b and c can be given as program arguments, otherwise they
  // will be read in dialog boxes.
  //
  // Note: with IntelliJ IDEA, program arguments can be passed via the
  //       corresponding field of the "Run/Debug configuration"
  //       (e.g. menu Run -> Edit configurations...)
  //----------------------------------------------------------------------------
  public static void main(String[] args) {
    int a, b, c;
    if (args.length == 3) {
      a = Integer.parseInt(args[0]);
      b = Integer.parseInt(args[1]);
      c = Integer.parseInt(args[2]);
    } else {
      a = SimpleIO.readInt("Value of a");
      b = SimpleIO.readInt("Value of b");
      c = SimpleIO.readInt("Value of c");
    }

    System.out.println("Equation: (" + a + ")x\u00B2 + (" + b + ")x + (" + c + ") == 0");

    // TODO
    if (a == 0) {
      if (b == 0 && c == 0) {
        System.out.println("Ensemble des nombres réels");
      } else if (b == 0) {
        System.out.println("Impossible !");
      } else {
        double x = -(c / b);
        System.out.println("x=(" + x + ")");
      }
    } else {
      double discriminant;
      discriminant = Math.pow(b, 2) - 4 * (a * c);

      if (discriminant > 0) {
        double x1 = ((-b) + Math.sqrt(discriminant)) / (2 * a);
        double x2 = ((-b) - Math.sqrt(discriminant)) / (2 * a);
        System.out.println("x1=(" + x1 + ")    x2=(" + x2 + ")");
      } else if (discriminant == 0) {
        double x = (double) (-b) / (2 * a);
        System.out.println("x=(" + x + ")");
      } else {
        double x = (double) (-b) / (2 * a);
        double i = (Math.sqrt(-discriminant)) / (2 * a);
        System.out.println("x1=(" + x + ") + (" + i + ")i   x2=(" + x + ") + (" + i + ")i");
      }
    }
  }
}
