package s08;

public class Ex0802a {
    public static void main(String[] args) {
        int[] t = new int[3];   //1
        int[] u = {5, 6, 7};    //2
        t[1] = 43;              //3
        u = t;                  //4
        u[2] = 2 * u[1];        //5
        u = null;               //6
        t[0] = t[2] - 1;        //7
    }
}