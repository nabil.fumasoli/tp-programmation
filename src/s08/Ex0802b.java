package s08;

public class Ex0802b {
    public static void main(String[] args) {
        int[] t = new int[3];              //1
        t[0] = 2;
        t[1] = 4;
        t[2] = 6;       //2
        t[0] = f(t);                        //5
    }

    public static int f(int[] u) {
        u[1] = u[0] + u[1];                 //3
        u[2] = u[1] + u[2];                 //4
        return u[2];
    }
}
